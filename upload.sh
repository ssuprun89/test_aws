sam build

aws cloudformation package --s3-bucket suprun-test-aws --template .aws-sam/build/template.yaml --output-template-file gen/template-generated.yaml

aws cloudformation deploy --s3-bucket suprun-test-aws --template gen/template-generated.yaml --stack-name test-aws --capabilities CAPABILITY_IAM
